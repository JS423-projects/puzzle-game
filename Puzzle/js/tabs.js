var tabButtons;
var tabs;
function switchTabs() {
	var elem = event.target;
	switch (elem) {
		case tabButtons[0]:
			tabButtons[0].style.backgroundColor = "#c9c7c7";
			tabButtons[1].style.backgroundColor = "#ebe7e7";
			tabs[0].style.display = "block";
			tabs[1].style.display = "none";
			break;

		case tabButtons[1]:
			tabButtons[1].style.backgroundColor = "#c9c7c7";
			tabButtons[0].style.backgroundColor = "#ebe7e7";
			tabs[1].style.display = "block";
			tabs[0].style.display = "none";
			break;
	}
}

function init() {
	var tabSection = document.getElementById("tabs");
	tabSection.addEventListener("click", switchTabs);
	tabButtons = tabSection.children;
	var gameStats = document.getElementById("Game_Setup");
	var statsTable = document.getElementById("Stats_table");
	tabs = [gameStats, statsTable];
}
document.addEventListener("DOMContentLoaded", init);
