var playerManager;
var chrono;
var utils;
var game;

function Utility() {
	this.generateRandomNumber = function (minValue, maxValue) {
		return Math.floor(Math.random() * (maxValue - minValue)) + minValue;
	};
	this.playAudio = function (path) {
		var audio = document.createElement("audio");
		audio.src = path;
		audio.play();
	};
	this.sampleEasyBoardTests = function () {
		var puzzleWidth = document.getElementById("Dimensions").value;
		var sampleTest;
		switch (puzzleWidth) {
			case "Three":
				sampleTest = [
					[1, 2, 3],
					[4, 0, 6],
					[7, 5, 8],
				];
				break;
			case "Four":
				sampleTest = [
					[1, 2, 3, 4],
					[5, 6, 7, 8],
					[9, 10, 0, 12],
					[13, 14, 11, 15],
				];
				break;
			case "Five":
				sampleTest = [
					[1, 2, 3, 4, 5],
					[6, 7, 8, 9, 10],
					[11, 12, 13, 14, 15],
					[16, 17, 18, 0, 20],
					[21, 22, 23, 19, 24],
				];
		}
		return sampleTest;
	};
	this.terminateGame = function (theStatus) {
		// Stop the clock
		clearInterval(chrono);

		// Remove tiles
		var board = document.querySelector(".puzzleBoard");
		board.textContent = "";

		// Add the player
		playerManager.storeGameStats(theStatus);

		// Reset values
		var playerName = document.getElementById("Player_Name_input");
		var dimension = document.getElementById("Dimensions");
		var moves = document.getElementById("moves");
		var seconds = document.getElementById("seconds");
		var minutes = document.getElementById("minutes");
		var hours = document.getElementById("hours");

		playerName.value = "";
		dimension.selectedIndex = 0;
		moves.value = 0;
		seconds.value = 0;
		minutes.value = 0;
		hours.value = 0;

		// Disable Play and Cancel Buttons
		utils.enableButton("Play_btn", true, "disabledButton");
		utils.enableButton("Cancel_btn", true, "disabledButton");

		// Disable tiles
		utils.enableTileClick(false);

		// Create new table with updated player info
		utils.showStats();
	};
	this.cancelPuzzlePlay = function () {
		utils.terminateGame(":cancelled");
		var cancelButton = document.getElementById("Cancel_btn");
		cancelButton.disabled = true;
	};
	this.checkFormFilled = function () {
		// For on key up
		var playerName = document.getElementById("Player_Name_input");
		var puzzleLevel = document.getElementById("Dimensions");
		var playBtn = document.getElementById("Play_btn");
		if (playerName.value.length != 0 && puzzleLevel.selectedIndex != 0) {
			utils.enableButton("Play_btn", false, "greenButton");
		} else {
			utils.enableButton("Play_btn", true, "disabledButton");
		}
	};
	this.enableTileClick = function (on) {
		var tiles = document.querySelectorAll(".puzzleBoard th");
		var choice = "none";
		if (on) {
			choice = "auto";
		}
		tiles.forEach(function (tile) {
			tile.style.pointerEvents = choice;
		});
	};
	this.enableButton = function (btnID, status, btnClass) {
		var btn = document.getElementById(btnID);
		btn.disabled = status;
		btn.setAttribute("class", btnClass);
	};
	this.showCrono = function (miliseconds = 1000) {
		var seconds = document.getElementById("seconds");
		var minutes = document.getElementById("minutes");
		var hours = document.getElementById("hours");

		var secs = 0;
		// NEED TO STORE IN A VARIABLE SO IT CAN BE STOPPED WITH clearInterval()
		chrono = setInterval(function () {
			secs++;
			seconds.value = secs;
			if (secs % 60 == 0) {
				secs = 0;
				seconds.value = secs;
				minutes.value = parseInt(minutes.value) + 1;

				if (parseInt(minutes.value) % 60 == 0) {
					minutes.value = 0;
					hours.value = parseInt(hours.value) + 1;
				}
			}
		}, miliseconds); // Change from 1000 to something like 0.0001 to make time go faster
	};
	this.showStats = function () {
		var table = document.getElementsByClassName("tableStats")[0];

		var gameCounter = playerManager.gameCounter - 1;
		var tr = document.createElement("tr");

		var number = document.createElement("td");
		var numberTxt = document.createTextNode(gameCounter + 1);
		number.appendChild(numberTxt);

		var name = document.createElement("td");
		var nameTxt = document.createTextNode(
			playerManager.listPlayers[gameCounter].name
		);
		name.appendChild(nameTxt);

		var dim = document.createElement("td");
		var dimTxt = document.createTextNode(
			playerManager.listPlayers[gameCounter].dimensions
		);
		dim.appendChild(dimTxt);

		var status = document.createElement("td");
		var statusTxt = document.createTextNode(
			playerManager.listPlayers[gameCounter].status
		);
		status.appendChild(statusTxt);

		var moves = document.createElement("td");
		var movesTxt = document.createTextNode(
			playerManager.listPlayers[gameCounter].moves
		);
		moves.appendChild(movesTxt);

		var duration = document.createElement("td");
		var durationTxt = document.createTextNode(
			playerManager.listPlayers[gameCounter].chrono
		);
		duration.appendChild(durationTxt);

		tr.append(number, name, dim, status, moves, duration);
		table.appendChild(tr);
	};
}

//----------------------------- Lucas' Code ---------------------------------------

function PlayerManager() {
	this.listPlayers = [];
	this.gameCounter = 0;

	this.storeGameStats = function (theStatus) {
		var playerName = document.getElementById("Player_Name_input");
		var dimension = document.getElementById("Dimensions");
		var moves = document.getElementById("moves");
		var seconds = document.getElementById("seconds");
		var minutes = document.getElementById("minutes");
		var hours = document.getElementById("hours");

		// Get time value
		var chrono =
			parseInt(seconds.value) +
			parseInt(minutes.value) * 60 +
			parseInt(hours.value) * 3600;

		var player = new Player(
			playerName.value,
			dimension.value,
			moves.value,
			chrono,
			theStatus
		);

		this.listPlayers.push(player);

		this.gameCounter++;
	};
}

function Player(userName, bDimension, numOfMoves, chrono, status) {
	this.name = userName;
	this.dimensions = bDimension;
	this.moves = numOfMoves;
	this.chrono = chrono;
	this.status = status;
}

function PuzzleGame() {
	this.puzzleWidth = document.getElementById("Dimensions").value;
	this.puzzleBoard = [];
	this.goalState = [];

	this.board1dEquivalent = [];

	this.createGoalState = function () {
		switch (this.puzzleWidth) {
			case "Three":
				this.goalState = [
					[1, 2, 3],
					[4, 5, 6],
					[7, 8, 0],
				];
				break;
			case "Four":
				this.goalState = [
					[1, 2, 3, 4],
					[5, 6, 7, 8],
					[9, 10, 11, 12],
					[13, 14, 15, 0],
				];
				break;
			case "Five":
				this.goalState = [
					[1, 2, 3, 4, 5],
					[6, 7, 8, 9, 10],
					[11, 12, 13, 14, 15],
					[16, 17, 18, 19, 20],
					[21, 22, 23, 24, 0],
				];
		}
	};

	this.createBoardStructure = function () {
		// Using random numbers

		var arrFiller = [];

		switch (this.puzzleWidth) {
			case "Three":
				this.puzzleBoard = [[], [], []];

				// arrFiller will be 3x3
				fillingArray(arrFiller, 3);

				fillingMultiDArray(this.puzzleBoard, arrFiller, 3);
				break;
			case "Four":
				this.puzzleBoard = [[], [], [], []];

				fillingArray(arrFiller, 4);

				fillingMultiDArray(this.puzzleBoard, arrFiller, 4);

				break;
			case "Five":
				this.puzzleBoard = [[], [], [], [], []];

				fillingArray(arrFiller, 5);

				fillingMultiDArray(this.puzzleBoard, arrFiller, 5);
		}

		// Testing
		// Make one Tile object for each element inside the array returned.
		//this.puzzleBoard = utils.sampleEasyBoardTests();

		for (var row = 0; row < this.puzzleBoard.length; row++) {
			for (
				var column = 0;
				column < this.puzzleBoard[row].length;
				column++
			) {
				// If the number at puzzleBoard[row][column] is 0, the tile is "emptyTile", otherwise it's filled.
				if (this.puzzleBoard[row][column] == 0) {
					this.puzzleBoard[row][column] = new Tile(
						row,
						column,
						"emptyTile",
						this.puzzleBoard[row][column]
					);
				} else {
					this.puzzleBoard[row][column] = new Tile(
						row,
						column,
						"filledTile",
						this.puzzleBoard[row][column]
					);
				}
			}
		}

		// Make a copy of the board as a 1D array to improve loop efficiency where needed.
		for (var i = 0; i < this.puzzleBoard.length; i++) {
			for (var j = 0; j < this.puzzleBoard[i].length; j++) {
				this.board1dEquivalent.push(this.puzzleBoard[i][j]);
			}
		}

		this.createGoalState();
		// At this point all Tiles are made and the board is filled. Just change HTML properties. (Call drawBoard).
		this.drawPuzzleBoard();
	};

	// Fills up every array in the multidimensional array and assures numbers don't repeat.
	function fillingMultiDArray(arrToFill, fillerArr, dimension) {
		// tracker is the index of elements in the fillerArr.
		var tracker = 0;

		for (var i = 0; i < arrToFill.length; i++) {
			// The param dimensions is the number of elements to put in each sub array.
			var numOfElements = dimension;

			while (numOfElements != 0) {
				arrToFill[i].push(fillerArr[tracker]);

				tracker++;
				numOfElements--;
			}
		}
	}

	// This function will check if the arrToFill doesn't surpass the dimension length.
	// If it's good it will generate a random number. If that number isn't in the array yet, push it to the array then run function again untill it's filled.
	function fillingArray(arrToFill, dimension) {
		// Ex: If the dimension is 3, stop when the length is greater than 9
		if (arrToFill.length >= Math.pow(dimension, 2)) {
			return;
		}

		// Generate a random number
		var randNum = utils.generateRandomNumber(0, Math.pow(dimension, 2));

		if (arrToFill.indexOf(randNum) < 0) {
			arrToFill.push(randNum);
		}

		fillingArray(arrToFill, dimension);
	}

	this.drawPuzzleBoard = function () {
		var board = document.querySelector(".puzzleBoard");

		// If there are no tiles in the board, create them
		if (board.children.length == 0) {
			var dimensions = this.puzzleWidth.toLowerCase();
			var numRows;
			switch (dimensions) {
				case "three":
					numRows = 3;
					break;
				case "four":
					numRows = 4;
					break;
				case "five":
					numRows = 5;
					break;
			}

			// Index used to look through logical puzzleBoard
			var index = 0;

			// Go through all rows
			for (var i = 0; i < numRows; i++) {
				var tr = document.createElement("tr");

				// Go throught each column since rows==columns
				for (var j = 0; j < numRows; j++) {
					var tile = document.createElement("th");

					// Set the tile number
					tile.innerHTML = this.board1dEquivalent[index].indexNumber;
					// Set the tile width
					tile.style.width = 100 / numRows + "%";
					tile.style.height = 100 / numRows + "%";

					index++;

					if (tile.innerHTML == 0) {
						tile.setAttribute("class", "emptyTile");
					} else {
						tile.setAttribute("class", "filledTile");
					}
					tr.appendChild(tile);
				}
				board.appendChild(tr);
			}
		}

		// Otherwise, just switch there numbers
		// At this point all Tiles are made and the board is filled. Just change HTML properties.
		else {
			var allTiles = document.querySelectorAll(".puzzleBoard th");

			for (var i = 0; i < allTiles.length; i++) {
				allTiles[i].innerHTML = this.board1dEquivalent[i].indexNumber;
			}

			for (var i = 0; i < allTiles.length; i++) {
				if (allTiles[i].innerHTML == 0) {
					allTiles[i].setAttribute("class", "emptyTile");
				} else {
					allTiles[i].setAttribute("class", "filledTile");
				}
			}
		}
	};

	this.swap2Tiles = function (tile1, tile2) {
		// Swapping tiles' internal information
		var tempTile1 = new Tile(
			tile1.row,
			tile1.col,
			tile1.tileType,
			tile1.indexNumber
		);

		tile1.tileType = tile2.tileType;
		tile1.indexNumber = tile2.indexNumber;

		tile2.tileType = tempTile1.tileType;
		tile2.indexNumber = tempTile1.indexNumber;

		var tempTile2 = new Tile(
			tile2.row,
			tile2.col,
			tile2.tileType,
			tile2.indexNumber
		);

		// Swapping tiles inside puzzleBoard
		this.puzzleBoard[tempTile2.row][tempTile2.col].tileType =
			tile2.tileType;
		this.puzzleBoard[tempTile2.row][tempTile2.col].indexNumber =
			tile2.indexNumber;

		this.puzzleBoard[tile1.row][tile1.col].tileType = tile1.tileType;
		this.puzzleBoard[tile1.row][tile1.col].indexNumber = tile1.indexNumber;
	};

	// Compare to GoalState and return true if the two states are identical, meaning game is over.
	this.match2States = function (board, goalState) {
		if (board.length != goalState.length) {
			throw new Error("The arrays are not of same length.");
		}

		// Go through every array in board
		for (var i = 0; i < board.length; i++) {
			// Compare the elements of each array in board to the elements of each array in goalState
			for (var j = 0; j < board[i].length; j++) {
				// If they're not all equal return false.
				if (board[i][j].indexNumber != goalState[i][j]) {
					return false;
				}
			}
		}
		return true;
	};

	this.getNeighbourTilesArr = function (tile) {
		var neighbours = [];

		for (var i = 0; i < this.puzzleBoard.length; i++) {
			for (var j = 0; j < this.puzzleBoard[i].length; j++) {
				// If true this means that the input tile is in this position in the 2D array.
				if (this.puzzleBoard[i][j].indexNumber == tile.indexNumber) {
					// If tile is at any corner it automatically has 2 -1s as neighbours.
					if (
						(i == 0 && j == 0) ||
						(i == 0 && j == this.puzzleBoard[i].length - 1) ||
						(i == this.puzzleBoard.length - 1 && j == 0) ||
						(i == this.puzzleBoard.length - 1 &&
							j == this.puzzleBoard[i].length - 1)
					) {
						neighbours.push(-1, -1);
					}

					// If it's the first or last row it has at least one -1 neighbour.
					else if (i == 0 || i == this.puzzleBoard.length - 1) {
						neighbours.push(-1);
					}

					// If it's the last element of a row it has at least one -1 neighbour.
					else if (j == 0 || j == this.puzzleBoard[i].length - 1) {
						neighbours.push(-1);
					}

					// Get top neighbour from the array above.
					if (i != 0) {
						neighbours.push(
							this.puzzleBoard[i - 1][j] /*.indexNumber*/
						);
					}

					// Get bottom neighbour from array below.
					if (i != this.puzzleBoard.length - 1) {
						neighbours.push(
							this.puzzleBoard[i + 1][j] /*.indexNumber*/
						);
					}

					// Get left neighbour.
					if (j != 0) {
						neighbours.push(
							this.puzzleBoard[i][j - 1] /*.indexNumber*/
						);
					}

					// Get right neighbour.
					if (j != this.puzzleBoard[i].length - 1) {
						neighbours.push(
							this.puzzleBoard[i][j + 1] /*.indexNumber*/
						);
					}
				}
			}
		}

		return neighbours;
	};

	this.processClickTile = function () {
		var tileToProcess;

		var tileNumber = event.target.innerHTML;

		// Check if the tile was a valid tile
		var emptyTile;

		// Get the tile equivalent in board1dEquivalent
		this.board1dEquivalent.forEach(function (tile, index, array) {
			if (tile.indexNumber == "" || tile.indexNumber == 0) {
				emptyTile = array[index];
			}
		});

		// Check if clicked target is one of the neighbours
		var isValidTile = false;
		var emptyTileNeighbours = this.getNeighbourTilesArr(emptyTile);
		emptyTileNeighbours.forEach(function (tile) {
			if (
				tile.indexNumber == tileNumber ||
				tileNumber == emptyTile.indexNumber
			) {
				isValidTile = true;
			}
		});

		// If it wasn't a valid tile, play the beep sound. Otherwise, switch the tiles
		if (!isValidTile) {
			utils.playAudio("sounds/beep-07.mp3");
		} else {
			// Use the equivalent 1d array of the board for efficiency
			for (var j = 0; j < this.board1dEquivalent.length; j++) {
				if (
					tileNumber == "" &&
					this.board1dEquivalent[j].indexNumber == 0
				) {
					tileToProcess = this.board1dEquivalent[j];
				}
				// The index Number of the empty tile is 0, so this conditioin wouldn't work.
				else if (this.board1dEquivalent[j].indexNumber == tileNumber) {
					tileToProcess = this.board1dEquivalent[j];
				}
			}

			var neighbours = this.getNeighbourTilesArr(tileToProcess);

			for (var i = 0; i < neighbours.length; i++) {
				if (neighbours[i] != -1 && neighbours[i].indexNumber == 0) {
					// Swap tiles
					this.swap2Tiles(tileToProcess, neighbours[i]);

					// Increase number of moves
					var moves = document.getElementById("moves").value;
					document.getElementById("moves").value = ++moves;

					// Draw Board
					this.drawPuzzleBoard();

					// If they won, finish the game.
					if (this.match2States(this.puzzleBoard, this.goalState)) {
						utils.playAudio("sounds/Firework.mp3");
						utils.terminateGame("Success");
					}
				}
			}
		}
	};

	//------------------------------------ Tile Object
	function Tile(row, column, tileType, indexNum) {
		// Get which level was chosen
		var puzzleLevel = document.getElementById("Dimensions").value;

		if (indexNum < 0) {
			throw new Error("Negative index number not valid.");
		}

		if (indexNum >= Math.pow(puzzleLevel, 2)) {
			indexNum = Math.pow(puzzleLevel, 2) - 1;
		}

		if (tileType != "emptyTile" && tileType != "filledTile") {
			throw new Error(
				"The tileType has to be either 'emptyTile' or 'filledTile'. Case sensitive."
			);
		}

		this.row = row;
		this.col = column;
		this.tileType = tileType;
		this.indexNumber = indexNum;
	}
}

//-------------------------------------- Main Stuff -------------------
function mainProgram() {
	// Enable Cancel Button
	utils.enableButton("Cancel_btn", false, "redButton");

	// Disable Play Button
	utils.enableButton("Play_btn", true, "disabledButton");

	// Create puzzle game with selected dimensions
	var puzzleLevel = document.getElementById("Dimensions").value;
	game = new PuzzleGame(puzzleLevel);
	game.createBoardStructure();

	// Add tiles click event to move them
	var tiles = document.querySelectorAll(".puzzleBoard th");
	tiles.forEach(function (tile) {
		tile.addEventListener("click", function () {
			game.processClickTile();
		});
	});

	// Enable clicking
	utils.enableTileClick(true);

	// Start the clock
	utils.showCrono();
}

function init() {
	// Initialize utility and playerManager
	utils = new Utility();
	playerManager = new PlayerManager();

	var playerName = document.getElementById("Player_Name_input");
	var puzzleLevel = document.getElementById("Dimensions");

	playerName.addEventListener("keyup", utils.checkFormFilled);
	puzzleLevel.addEventListener("mouseup", utils.checkFormFilled);

	var playBtn = document.getElementById("Play_btn");
	playBtn.addEventListener("click", mainProgram);

	var cancelBtn = document.getElementById("Cancel_btn");
	cancelBtn.addEventListener("click", utils.cancelPuzzlePlay);
}

document.addEventListener("DOMContentLoaded", init);
